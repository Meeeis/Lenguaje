var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
/*
var index = require('./routes/index');
var users = require('./routes/users');
*/


var dbUrl = "mongodb://dbuser:asdasd@ds147469.mlab.com:47469/itemslol?authMechanism=SCRAM-SHA-1";
const db = require('monk')(dbUrl);
const itemslol = db.get('itemslol');

const users = db.get('users');

var passport = require('passport');
var JwtStrategy = require ('passport-jwt').Strategy;
var ExtractJwt = require ('passport-jwt').ExtractJwt;
var jwt = require ('jsonwebtoken');

var app = express();


var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
opts.secretOrKey = "ASDASDASD";

passport.use(new JwtStrategy(opts, function (jwt_payload, done){
    users.findOne({"_id":jwt_payload._id})
        .then(function (user) {
            if(user){
                done(null, user);
            }else {
                done(null, false);
            }
        }).catch(function (error){
        console.log("ERROR "+ error);
    });
}));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors());

/*
app.use('/', index);
app.use('/users', users);
*/

app.post('/api/auth/register', function (req, res){
    var data = req.body;

    if (!data.username || !data.password){
        res.json({success: false, msg: 'PLEASE BLABLABLA'});
    } else {
        users.findOne({username: data.username})
            .then(function (user){
                console.log(user);

                if(user){
                    users.insert(data);
                    return res.json({success: true, msg:'USUARIO CREADO' });
                }else {
                    return res.json ({success: false, msg: 'A FREGAR'});
                }
            });
    }
});

app.post('/api/auth/login', function (req, res){
    var data = req.body;

    users.findOne({username: data.username})
        .then(function (user){
            if(!user){
                res.status(401).send({success: false, msg: 'USUARIO NO ENCONTRADO, TORNA-HI'});
            } else {
                if (user.password === data.password){
                    var token = jwt.sign(
                        {
                            "_id":user._id,
                            "username": user.username
                        },
                        opts.secretOrKey
                    );

                    res.json({success: true, token: 'JWT '+ token});
                } else {
                    res.status(401).send({success: false, msg: 'CONTRASEÑA INCORRECTA MAN'});
                }
            }
        })
});

app.get('/api/item/', function(req, res){
    itemslol.find({})
        .then(function (data){
            console.log(data);
            res.json({item: data});
        }).catch(function (err) {
        console.log(err);
    });
});

app.post('/api/item', function(req, res){
    var data = req.body;

    itemslol.insert(data)
        .then(function (data){
            res.json({item: data});
        }).catch(function (err) {
        console.log(err);
    });
});

app.get('/api/item/:id', function(req, res){
    var _id = (req.params.id);

    itemslol.find({"_id": _id})
    .then(function (data){
            res.json(data);
        }).catch(function (err) {
        console.log(err);
    });
});

app.post('/api/item/:id', function(req, res){
    var _id = req.params.id;
    var data = req.body;

    itemslol.update({"_id": _id}, data)
        .then(function (data){
            res.json(data);
        }).catch(function (err) {
        console.log(err);
    });
});

app.delete('/api/item/:id', passport.authenticate('jwt', {session: false}), function(req, res) {
    if (req.user) {
        console.log(req.user.username)

        var _id = req.params.id;

        console.log(_id)
        itemslol.remove({"_id": _id})
            .then(function (data) {
                console.log("Entra")

                res.json(data);
            }).catch(function (err) {
            console.log("ERROR " + err);
        });
    } else {
        return res.status(403).send({success: false, msg: 'QUE NO.'});
    }
});




app.get('/rss', function (req, res) {
    itemslol.find({})
        .then(function (data) {
            console.log(data);
            var rss =
                `<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0">

  <channel>
    <title>ITEMS</title>
    <link>localhost</link>
    <description>Lista de items</description>`;

            for (var i = 0; i < data.length; i++) {
                rss += `
        <item>
          <title>${data[i].id}</title>
          <link>${data[i].itemName}</link>
          <description>${data[i].itemDescription}</description>
          <price>${data[i].itemPrice}</price>
          <pic>${data[i].itempic}</pic>
        </item>`;
            }

            rss += `
  </channel>
</rss>`;

            res.set('Content-Type', 'text/xml');
            res.send(rss);
        }).catch(function (error) {
        console.log(error);
    });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;